import datascout as ds
import numpy as np

my_dict = dict()
my_dict['device1'] = np.random.rand(10)
my_dict['device2'] = dict()
my_dict['device2']['value'] = np.random.rand(10,2)
my_dict['device2']['string'] = 'test dict'

#### ONLINE data conversion
ds.dict_to_awkward(my_dict)
ds.dict_to_pyarrow(my_dict)
ds.dict_to_pandas(my_dict)

#### ToDISK data conversion
# Parquet
ds.dict_to_parquet(my_dict, "my_test_data.parquet")
my_dict_load = ds.parquet_to_dict("my_test_data.parquet")
# This will return no output if the two dictionaries contain the same data
ds._compare_data(my_dict, my_dict_load)

# Pickle
ds.dict_to_pickle(my_dict, "my_test_data.pickle")
my_dict_load = ds.pickle_to_dict("my_test_data.pickle")
ds._compare_data(my_dict, my_dict_load)

# JSON - no exact data preservation
ds.dict_to_json(my_dict, "my_test_data.json")
my_pandas_load = ds.json_to_pandas("my_test_data.json")
my_dict_from_json = ds.pandas_to_dict(my_pandas_load)
ds._compare_data(my_dict, my_dict_from_json)

# note: going to pandas and back to dict does keep the data integrity
my_pandas = ds.dict_to_pandas(my_dict)
my_dict_from_pandas = ds.pandas_to_dict(my_pandas)
ds._compare_data(my_dict, my_dict_from_pandas)


