import datascout as ds
import math
from datetime import timezone
import datetime
import numpy as np


def convert_2d_array(my_array):
    if len(np.shape(my_array)) == 1:
        return my_array
    list = []
    for jj in range(np.shape(my_array)[0]):
        list.append(my_array[jj, :])
    return list


def old2new(my_dict, verbose=False):
    for device_name, device_value in my_dict.items():
        if verbose:
            print(device_name)
        device_value["header"]["acqStamp"] = np.int64(
            device_value["header"]["acqStamp"].replace(tzinfo=timezone.utc).timestamp()
            * 1e9
        )
        device_value["header"]["cycleStamp"] = (
            np.int64(
                device_value["header"]["cycleStamp"]
                .replace(tzinfo=timezone.utc)
                .timestamp()
                * 1e6
            )
            * 1e3
        )
        device_value["header"]["setStamp"] = np.int64(
            device_value["header"]["setStamp"].replace(tzinfo=timezone.utc).timestamp()
            * 1e9
        )
        if (not type(device_value["value"]) == dict) and (
            not type(device_value["value"]) == list
        ):
            if math.isnan(device_value["value"]):
                device_value["value"] = "no data"
            if type(device_value["value"]) == np.ndarray:
                device_value["value"] = convert_2d_array(device_value["value"])
        if type(device_value["value"]) == dict:
            for value_name, value_value in device_value["value"].items():
                if verbose:
                    print(device_value["value"][value_name])
                if type(device_value["value"][value_name]) == np.ndarray:
                    device_value["value"][value_name] = convert_2d_array(value_value)
    return my_dict


# my_parquet = '/eos/project/l/liu/datascout/parquet_file/2021.04.30.01.54.47.454151.parquet'
my_dict_file = (
    "/eos/project/l/liu/datascout/pickle_files/2021.04.30.01.54.47.454151.pkl"
)

# ds.parquet_to_dict(my_parquet)
my_dict = ds.pickle_to_dict(my_dict_file)

a = old2new(my_dict[0])

# ONLINE
ds.dict_to_awkward(a)
ds.dict_to_pyarrow(a)
ds.dict_to_pandas(a)

# OFFLINE
ds.dict_to_parquet(a, "test")
ds.dict_to_json(a, "test")
ds.dict_to_pickle(a, "test")

#
print("parquet_to_pyarrow")
ds.parquet_to_pyarrow("test.parquet")

print("parquet_to_pandas")
ds.parquet_to_pandas("test.parquet")

print("parquet_to_awkward")
ds.parquet_to_awkward("test.parquet")
