import datascout as ds
import datetime

reference_stamp = 1645617457640238425
reference_time  = datetime.datetime(2022, 2, 23, 12, 57, 37, 640238)

myDateTime_utc = ds.unixtime_to_datetime(reference_stamp, time_zone = 'utc')
myDateTime_local = ds.unixtime_to_datetime(reference_stamp, time_zone = 'local')
myDateTime_cern = ds.unixtime_to_datetime(reference_stamp, time_zone = 'CERN')
myDateTime_unaware = ds.unixtime_to_datetime(reference_stamp, time_zone = None)

# the unaware time should be as the ref time created above
assert(myDateTime_unaware == reference_time)

# Note that datetime object are precise only down to the microsecond!
reference_stamp_us_precision = round(reference_stamp/1000.)*1000
assert(ds.datetime_to_unixtime(myDateTime_utc)     == reference_stamp_us_precision)
assert(ds.datetime_to_unixtime(myDateTime_local)   == reference_stamp_us_precision)
assert(ds.datetime_to_unixtime(myDateTime_cern)    == reference_stamp_us_precision)
assert(ds.datetime_to_unixtime(myDateTime_unaware) == reference_stamp_us_precision)

# can also work with strings
reference_time_str = ds.unixtime_to_string(reference_stamp, time_zone = 'cern')
print(ds.string_to_datetime(reference_time_str))
assert(ds.string_to_unixtime(reference_time_str) == reference_stamp_us_precision)


