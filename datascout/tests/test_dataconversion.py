"""
Try different functions on an example dataset

"""
import datascout
import numpy as np
import copy
import os


def generate_data_dict():
    """
    Simply generate a dictionary with some values that should be compatible with this package.
    Note: only 'device1' contains random data...
    """
    return {
        "device1": {
            "value": {
                "property1": np.int8(np.random.rand(1)[0] * 10 ** 2),
                "property2": np.int8(np.random.rand(1, 43) * 10 ** 2),
                "property3": np.int8(np.random.rand(10, 3) * 10 ** 2),
                "property4": np.int8(np.random.rand(50, 1) * 10 ** 2),
            },
            "header": {
                "acqStamp": np.int64(np.random.rand(1)[0] * 10 ** 12),
                "cycleStamp": np.int64(np.random.rand(1)[0] * 10 ** 12),
            },
            "exception": "",
        },
        "device2": {
            "value": np.array([[1, 12], [4, 5], [1, 2]], dtype=np.int16),
            "header": {"acqStamp": np.int64(44444), "cycleStamp": np.int64(3455445)},
            "exception": "",
        },
        "device3": {
            "value": "",
            "header": {"acqStamp": np.int64(44444), "cycleStamp": np.int64(0)},
            "exception": "Cipolla",
        },
        "device4": {
            "value": {
                "property5": "This is string",
                "property6": np.array(
                    ["my", "list"], dtype=str
                ),  # np.str_ or object? -> np.str_!
                "property7": np.array([["my"], ["list"], ["long"]], dtype=str),
                "property8": np.array(
                    [["my", "list"], ["of", "more"], ["val", "string"]], dtype=str
                ),
            },
            "header": {"acqStamp": np.int64(55555), "cycleStamp": np.int64(3455445)},
            "exception": "",
        },
        "device5": {
            "value": {
                "property9": {
                    "JAPC_FUNCTION": {
                        "X": np.array([1, 2, 3, 4], dtype=np.float64),
                        "Y": np.array([14, 2, 7, 5], dtype=np.float64),
                    }
                },
                "property6": np.array(
                    [
                        {
                            "JAPC_FUNCTION": {
                                "X": np.array([1, 2], dtype=np.float64),
                                "Y": np.array([14, 2], dtype=np.float64),
                            }
                        },
                        {
                            "JAPC_FUNCTION": {
                                "X": np.array([3, 4], dtype=np.float64),
                                "Y": np.array([7, 5], dtype=np.float64),
                            }
                        },
                    ],
                    dtype=object,
                ),
            },
            "header": {"acqStamp": np.int64(4444444), "cycleStamp": np.int64(0)},
            "exception": "",
        },
        "device6": {
            "value": {
                "JAPC_FUNCTION": {
                    "X": np.array([1, 2, 3, 4], dtype=np.float64),
                    "Y": np.array([14, 2, 7, 5], dtype=np.float64),
                }
            },
            "header": {"acqStamp": np.int64(4455444), "cycleStamp": np.int64(0)},
            "exception": "",
        },
        "device7": {
            "value": {
                "property10": {"JAPC_ENUM": {"code": np.int64(2), "string": "piero"}},
                "property11": np.array(
                    [
                        {"JAPC_ENUM": {"code": np.int64(3), "string": "carlo"}},
                        {"JAPC_ENUM": {"code": np.int64(4), "string": "micio"}},
                    ],
                    dtype=object,
                ),
                "property12": {
                    "JAPC_ENUM_SET": {
                        "codes": np.array([2, 8], dtype=np.int64),
                        "aslong": np.int64(123),
                        "strings": np.array(["nieva", "po"], dtype=str),
                    }
                },  # np.str_
                "property13": np.array(
                    [
                        {
                            "JAPC_ENUM_SET": {
                                "codes": np.array([7, 44], dtype=np.int64),
                                "aslong": np.int64(123),
                                "strings": np.array(["nieva", "po"], dtype=str),
                            }
                        },
                        {
                            "JAPC_ENUM_SET": {
                                "codes": np.array([5, 6], dtype=np.int64),
                                "aslong": np.int64(77),
                                "strings": np.array(["nettuno", "plutone"], dtype=str),
                            }
                        },
                    ],
                    dtype=object,
                ),
                "property14": np.array(
                    [
                        {
                            "JAPC_ENUM_SET": {
                                "codes": np.array([], dtype=np.int64),
                                "aslong": np.int64(0),
                                "strings": np.array([], dtype=str),
                            }
                        },
                        {
                            "JAPC_ENUM_SET": {
                                "codes": np.array([5, 6], dtype=np.int64),
                                "aslong": np.int64(77),
                                "strings": np.array(["nettuno", "plutone"], dtype=str),
                            }
                        },
                    ],
                    dtype=object,
                ),
            },
            "header": {"acqStamp": np.int64(44333444), "cycleStamp": np.int64(0)},
            "exception": "",
        },
        "device8": {
            "value": {
                "JAPC_ENUM_SET": {
                    "codes": np.array([2, 8], dtype=np.int64),
                    "aslong": np.int64(123),
                    "strings": np.array(["nieva", "po"], dtype=str),
                }
            },
            "header": {"acqStamp": np.int64(4), "cycleStamp": np.int64(0)},
            "exception": "no data for xxxx",
        },
        "device9": {
            "value": {"cipolla": np.array([], dtype=str)},
            "header": {"acqStamp": np.int64(4), "cycleStamp": np.int64(0)},
            "exception": "no data for xxxx",
        },
    }


def generic_test_data_conversion(my_data_dict):
    # make a reference copy of selected dict
    my_data_dict_ref = copy.deepcopy(my_data_dict)

    # go to panda and back without altering initial data
    my_pandas = datascout.dict_to_pandas(my_data_dict)
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    my_data_back = datascout.pandas_to_dict(my_pandas)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)

    # go to pyarrow and back without altering initial data
    my_pyarrow = datascout.dict_to_pyarrow(my_data_dict)
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    my_data_back = datascout.pyarrow_to_dict(my_pyarrow)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)

    # go to awkward and back without altering initial data
    my_ak = datascout.dict_to_awkward(my_data_dict)
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    my_data_back = datascout.awkward_to_dict(my_ak)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)

    # a long chain
    my_data_back = datascout.awkward_to_dict(
        datascout.pandas_to_awkward(
            datascout.pyarrow_to_pandas(datascout.dict_to_pyarrow(my_data_dict))
        )
    )
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)


def generic_test_save_load(tmpdir, my_data_dict):
    # make a reference copy of selected dict
    my_data_dict_ref = copy.deepcopy(my_data_dict)

    # define temporary filename
    temp_filename_parquet = os.path.join(str(tmpdir), "test.parquet")
    temp_filename_pickle = os.path.join(str(tmpdir), "test.pkl")

    # go to parquet
    datascout.dict_to_parquet(my_data_dict, temp_filename_parquet)
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    my_data_back = datascout.parquet_to_dict(temp_filename_parquet)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)

    # go to pickle
    datascout.dict_to_pickle(my_data_dict, temp_filename_pickle)
    datascout._compare_data(my_data_dict, my_data_dict_ref, use_assert=True)
    my_data_back = datascout.pickle_to_dict(temp_filename_pickle)
    datascout._compare_data(my_data_back, my_data_dict_ref, use_assert=True)


def test_data_conversion():
    # generate dataset
    my_data_dict = generate_data_dict()
    # test it
    generic_test_data_conversion(my_data_dict)


def test_data_array_conversion():
    # generate dataset
    my_data_dict = [generate_data_dict(), generate_data_dict(), generate_data_dict()]
    # test it
    generic_test_data_conversion(my_data_dict)


def test_save_load(tmpdir):
    # generate dataset
    my_data_dict = generate_data_dict()
    # test it
    generic_test_save_load(tmpdir, my_data_dict)


def test_save_load_array(tmpdir):
    # generate dataset
    my_data_dict = [generate_data_dict(), generate_data_dict(), generate_data_dict()]
    # test it
    generic_test_save_load(tmpdir, my_data_dict)


# Function above can be locally tests as:
"""
from pathlib import Path
tmpdir=Path('.')
test_save_load(tmpdir)
test_save_load_array(tmpdir)
"""
