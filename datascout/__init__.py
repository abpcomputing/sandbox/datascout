"""
Sweet functions for data conversion and data writing to disk

"""
# Note: the text above goes into the API documentation

from ._version import version as __version__


# to look at pyarrow, typically not used by a user,
# but key functions for this package
from ._datascout import dict_to_pyarrow
from ._datascout import pyarrow_to_parquet
from ._datascout import parquet_to_pyarrow
from ._datascout import pyarrow_to_dict
from ._datascout import pyarrow_to_pandas

# for the user
from ._datascout import dict_to_pandas
from ._datascout import dict_to_awkward
from ._datascout import dict_to_parquet
from ._datascout import dict_to_pickle
from ._datascout import dict_to_json

# not so interesting, but provided for convenience
from ._datascout import json_to_pandas

# coming back
from ._datascout import pandas_to_dict
from ._datascout import awkward_to_dict
from ._datascout import parquet_to_dict
from ._datascout import pickle_to_dict

# between pandas and awkward
from ._datascout import pandas_to_awkward
from ._datascout import awkward_to_pandas

# reading from parquet to pandas/awkward without type loss
from ._datascout import parquet_to_pandas
from ._datascout import parquet_to_awkward

# other hidden functions that could be useful for debug
from ._datascout import _find_lists
from ._datascout import _compare_data

# saving
from ._datascout import save_dict

# function to address different file schemas
from ._datascout import split_schemas
from ._datascout import compare_schema

# functions to convert date/times/cyclestamps...
from ._timescout import unixtime_to_datetime
from ._timescout import datetime_to_unixtime
from ._timescout import string_to_datetime
from ._timescout import string_to_unixtime
from ._timescout import unixtime_to_string