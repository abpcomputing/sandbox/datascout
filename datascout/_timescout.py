"""
Implementation of sweet functions to convert time data from one format to another
"""
import pytz
import datetime

def unixtime_to_datetime(unixtime_ns, time_zone = 'CERN'):
    '''Converts a timestamp in ns from epoch (UTC) to a datetime on the given time_zone

    Note, a timezone called "CERN" (default) can be used as timezone of Geneva area.

    Args:
         unixtime_ns (int): ns from epoch
         time_zone (string): desired timezone for the output (default='CERN')
    
    Returns:
         (datetime): datetime object representing the given timestamp
    '''
    if isinstance(time_zone, str):
        if time_zone.upper() == 'UTC':
            time_zone = pytz.utc
        elif time_zone.upper() == "LOCAL":
            time_zone = datetime.datetime.utcnow().astimezone().tzinfo
        elif time_zone.upper() == 'CERN':
            time_zone = pytz.timezone("Europe/Zurich")
    elif isinstance(time_zone, datetime.tzinfo) or time_zone is None:
        time_zone = time_zone
    else:
        print("Unknown timeZone argument: {0}. Falling back on UTC time.".format(time_zone))
        time_zone = pytz.utc

    return datetime.datetime.fromtimestamp(unixtime_ns / 1e9, tz=time_zone)

def datetime_to_unixtime(date_time):
    '''Converts a datetime object into a unixtimestamp in ns

    Note that datetime objects are precise only down to the microsecond.

    Args:
         date_time (datetime): datetime object to convert
    
    Returns:
         (int): ns from epoch UTC
    '''
    if date_time.tzinfo is None:
        epoch = datetime.datetime.fromtimestamp(0, tz=None)
    else:
        epoch = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)
    return int((date_time-epoch).total_seconds()*1e6)*1000

def string_to_datetime(date_string):
    '''Uses standard datetime function to parse a string in the ISO format

    Examples:
      >>> string_to_datetime('2022-01-21 15:55:34.123555')
      datetime.datetime(2022, 1, 21, 15, 55, 34, 123555, tzinfo=datetime.timezone(datetime.timedelta(seconds=3600), 'CET'))

    Args:
         date_string (string): string to parse 

    Returns:
         (datetime): datetime object with timezone representing the input string date and time
    '''

    return datetime.datetime.fromisoformat(date_string).astimezone()

def string_to_unixtime(date_string):
    '''Simple parse of ISO string and get a unix timestamp

    Args:
         date_string (string): date+time in ISO string format
    
    Returns:
         (int): unix timestamp in ns
    '''
    return datetime_to_unixtime(string_to_datetime(date_string))

def unixtime_to_string(unixtime_ns, time_zone = 'CERN', format = '%Y-%m-%d %H:%M:%S.%f'):
    '''
    Converts a unix timestamp in ns into a string on a given timezone and format

    Args:
         unixtime_ns (int): timestamp in ns to convert
         time_zone (string): timezone to use for the conversion (default='CERN') 
         format (string): (default='%Y-%m-%d %H:%M:%S.%f') 
    
    Returns:
         (string): string representation of the input timestamp
    '''
    return unixtime_to_datetime(unixtime_ns, time_zone=time_zone).strftime(format)

