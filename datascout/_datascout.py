"""
Implementation of sweet functions to convert data from one type to another

"""
import numpy as np
import pandas as pd
import awkward as ak
import pyarrow.parquet as pq
import pyarrow as pa
import pickle
import datetime
import copy
import os
import scipy
import glob
from pathlib import Path


"""
######
# Functions needed to split 2D arrays
"""
def _split_2D_array(val, in_memory=False, split_to_list=False, verbose=False):
    """It converts numpy 2D arrays into either 1D arrays or list of 1D arrays

    Args:
        val (numpy.ndarray): the array to convert
        in_memory (bool): data is not copied but just represented in a different form (default=False)
        split_to_list (bool): data is split in a 1D list instead of 1D object array (default=False)
        verbose (bool): print some information when data is split (default=False)

    Returns:
        the split value or the original value if the input was not of the right type or did not need to be split.
    """
    if (type(val) == np.ndarray) and len(np.shape(val)) == 2:
        if not in_memory:
            val = copy.deepcopy(val)
            if verbose:
                print("made a copy of " + str(val))
        if split_to_list:
            newVal = list(val)
        else:
            # (TODO: probably to be done better!!!)
            auxDim0 = np.shape(val)[0]
            newVal = ''
            if auxDim0 == 0:
                if verbose:
                    print('detected empty 2D array')
                newVal = np.empty(np.shape(val), dtype=object)
            else:
                # split val, without making data copy
                auxData = np.split(np.ravel(val), auxDim0)
                # put it in object array
                newVal = np.empty((auxDim0,), dtype=object)
                for i in range(auxDim0):
                    newVal[i] = auxData[i]
        if verbose:
            print("     -----    ")
            print(str(val) + " (" + str(type(val)) + ")")
            print(" -> converted to -> ")
            print(str(newVal) + " (" + str(type(newVal)) + ")")
        return newVal
    else:
        return val


def _convert_dict_list(data, in_memory=False, split_to_list=False, verbose=False):
    """
    Parse the input data, which should be a list or a dict, and convert all 2D arrays into either
    - 1D object array of 1D arrays
    - 1D list of 1D arrays

    If in_memory=True (default False), it changes the data in memory.
    In any case, the modified data is returned.

    NOTE: The conversion is done such to reduce to minimum (I think) data copy: i.e. the actual data
    is not copied (or copies are reduced to the minimum).
    It is up to the user to make a deepcopy, if desired, of the data before and/or after conversion.

    """
    if in_memory is False:
        data = copy.copy(data)
    if type(data) == list:
        for i in range(len(data)):
            if type(data[i]) == list or type(data[i]) == dict:
                data[i] = _convert_dict_list(data[i])
            elif type(data[i]) == np.ndarray:
                data[i] = _split_2D_array(
                    data[i],
                    in_memory=in_memory,
                    split_to_list=split_to_list,
                    verbose=verbose,
                )
    elif type(data) == dict:
        for key in data.keys():
            if type(data[key]) == list or type(data[key]) == dict:
                data[key] = _convert_dict_list(data[key])
            elif type(data[key]) == np.ndarray:
                data[key] = _split_2D_array(
                    data[key],
                    in_memory=in_memory,
                    split_to_list=split_to_list,
                    verbose=verbose,
                )
    return data


"""
######
# Functions needed to re-merge 1D arrays of 1D arrays into 2D arrays
"""


def _merge_to_2D(val, string_as_obj=False, verbose=False):
    """
    _merge_to_2D(val, string_as_obj=False, verbose=False)

    It converts back numpy arrays of "object" dtype into 2D arrays.
    By construction, if conversion actually occurs, this operation makes a copy of
    the data (probably with some exceptions)

    string_as_obj=False (default):
    This options (if enabled) makes sure that the returned object is a 2D array of "object"
    data type in case of string arrays. This is necessary in case you want to edit one string
    of the array without it being cut...
    """
    if ((type(val) == np.ndarray) and val.dtype == object) or (type(val) == list):
        newVal = np.stack(val)
        # fix subtle issue with strings which I am assuming are arrays of objects
        if string_as_obj and (newVal.dtype.type is np.str_):
            newVal = newVal.astype(object)
        if verbose:
            print("     -----    ")
            print(str(val) + " (" + str(type(val)) + ")")
            print(" -> reverted to -> ")
            print(str(newVal) + " (" + str(newVal.dtype) + ")")
        return newVal
    else:
        return val


def _revert_dict_list(data, in_memory=False, string_as_obj=False, verbose=False):
    """
    Parse the input data, which should be a list or a dict, and convert all 1D arrays of "object" type
    into 2D arrays of the proper data type.

    If string_as_obj=True (default=False), the obtained 2D arrays of strings are converted into 2D arrays
    of "object" dtype.

    If in_memory=True (default False), it changes the data in memory.
    In any case, the modified data is returned.

    NOTE: The conversion is done such to reduce to minimum (I think) data copy: i.e. the actual data
    is not copied (or copies are reduced to the minimum).
    It is up to the user to make a deepcopy, if desired, of the data before and/or after conversion.

    """

    if in_memory is False:
        data = copy.copy(data)
    if type(data) == list:
        for entry in data:
            if type(entry) == dict:
                _revert_dict_list(entry)
            elif type(entry) == list or type(entry) == np.ndarray:
                entry = _merge_to_2D(
                    entry, string_as_obj=string_as_obj, verbose=verbose
                )
                if len(entry) > 0 and isinstance(entry.flatten()[0], dict):
                    for nasted_data in entry.flatten():
                        _revert_dict_list(nasted_data)
    elif type(data) == dict:
        for key in data.keys():
            if type(data[key]) == dict:
                _revert_dict_list(data[key])
            elif type(data[key]) == list or type(data[key]) == np.ndarray:
                data[key] = _merge_to_2D(
                    data[key], string_as_obj=string_as_obj, verbose=verbose
                )
                if len(data[key]) > 0 and isinstance(data[key].flatten()[0], dict):
                    for nasted_data in data[key].flatten():
                        _revert_dict_list(nasted_data)
    return data


"""
#
# CORE function of this project: it allows to convert a pyarrow object into a dict
#
"""


def _convert_parrow_data(
    data, treat_str_arrays_as_str=True, use_list_for_2D_array=False
):
    """
    _convert_parrow_data(data)

    it extract data from a pyarrow object to a "standard" pyjapcscout-like dict dataset,
    i.e. a dictionary with only not null numpy objects/arrays and no lists (but if you enable use_list_for_2D_array)

    if treat_str_arrays_as_str (default=True) it will try to preserve str data type also for arrays

    if use_list_for_2D_array (default=False) it will try to use lists of 1D arrays instead of 2D arrays

    Typically the output should be a `dict`. If, however, one is trying to convert more complex structures
    like a pyarrow Table or StructArray, the output will be a list of dictionaries if more than one data
    records are found.
    """
    if isinstance(data, pa.lib.Table):
        output = []
        for irow in range(data.num_rows):
            outputRow = dict()
            for column in data.column_names:
                outputRow[column] = _convert_parrow_data(
                    data[column][irow], treat_str_arrays_as_str, use_list_for_2D_array
                )
            output.append(outputRow)
        if len(output) == 1:
            return output[0]
        else:
            return output
    elif isinstance(data, pa.StructArray):
        output = []
        for row in data:
            output.append(
                _convert_parrow_data(
                    row, treat_str_arrays_as_str, use_list_for_2D_array
                )
            )
        if len(output) == 1:
            return output[0]
        else:
            return output
    elif isinstance(data, pa.StructScalar):
        output = dict()
        for item in data.items():
            output[item[0]] = _convert_parrow_data(
                item[1], treat_str_arrays_as_str, use_list_for_2D_array
            )
        return output
    elif isinstance(data, pa.ListScalar):
        if isinstance(data.type.value_type, pa.lib.ListType):
            aux_dtype = data.type.value_type.value_type.to_pandas_dtype()
            if treat_str_arrays_as_str and data.type.value_type.value_type.equals(
                pa.string()
            ):
                # actually a string! not a generic object....
                aux_dtype = np.str_
            return np.array(data.as_py(), dtype=aux_dtype)
        elif isinstance(data.type.value_type, pa.lib.DataType):
            if isinstance(data.type.value_type, pa.lib.StructType):
                if use_list_for_2D_array:
                    output = []
                    for auxValue in data.values:
                        output.append(
                            _convert_parrow_data(auxValue),
                            treat_str_arrays_as_str,
                            use_list_for_2D_array,
                        )
                    return output
                else:
                    output = np.empty((len(data.values),), dtype=object)
                    for i, auxValue in enumerate(data.values):
                        output[i] = _convert_parrow_data(
                            auxValue, treat_str_arrays_as_str, use_list_for_2D_array
                        )
                    return output
            else:
                # could be a 1D array of some data type
                aux_dtype = data.type.value_type.to_pandas_dtype()
                if treat_str_arrays_as_str and data.type.value_type.equals(pa.string()):
                    # actually a string! not a generic object....
                    aux_dtype = np.str_
                return np.array(data.as_py(), dtype=aux_dtype)
        else:
            print("Zzzuth...")
            return data
    elif issubclass(type(data), pa.lib.Scalar):
        # horrible casting!... did not find a better way....
        return data.type.to_pandas_dtype()(data.as_py())
    else:
        print("Sigh... unknown data type: " + str(type(data)))
        return data


"""
######
# Some important functions not so interesting for the standard user, but fundamental
"""


def dict_to_pyarrow(input_dict):
    """Convert a dictionary into a PyArrow Array

    Args:
        input_dict (dict): the dictionary to convert

    Returns:
        (PyArrow): the data converted as PyArrow Array
    """
    my_data_dict_converted = _convert_dict_list(
        input_dict, in_memory=False, split_to_list=False, verbose=False
    )
    if not isinstance(my_data_dict_converted, list):
        my_data_dict_converted = [my_data_dict_converted]
    return pa.Table.from_pandas(pd.DataFrame(my_data_dict_converted))


def pyarrow_to_parquet(input_pa, filename):
    """Save a given PyArrow Array into a parquet file

    Args:
        input_pa (dict): the PyArrow Array to save
        filename (string): the file name (with its path)
    """
    pq.write_table(input_pa, filename)


def parquet_to_pyarrow(filename):
    """Loads a parquet file as PyArrow Array

    Args:
        filename (string): the file name (with its path)

    Returns:
        (PyArrow): the loaded file as PyArrow Array
    """
    return pq.read_table(filename)


def pyarrow_to_dict(input_pa):
    """Convert a PyArrow Array into a dictionary

    Args:
        input_pa (PyArrow): the PyArrow Array to convert

    Returns:
        (dict): the data converted as dict
    """
    return _convert_parrow_data(input_pa)


def pyarrow_to_pandas(input_pa):
    """Convert a PyArrow Array into a Pandas DataFrame

    Args:
        input_pa (PyArrow): the PyArrow Array to convert

    Returns:
        (DataFrame): the data converted as Pandas DataFrame
    """
    return dict_to_pandas(pyarrow_to_dict(input_pa))


"""
####### The functions interesting for the user are the following ones:
"""


def dict_to_pandas(input_dict):
    """Convert a dictionary or list of dictionaries into a pandas DataFrame

    Args:
        input_dict (dict): the dictionary to convert

    Returns:
        (DataFrame): the data converted as Pandas DataFrame
    """
    if not isinstance(input_dict, list):
        input_dict = [input_dict]
    return pd.DataFrame(input_dict)


def dict_to_awkward(input_dict):
    """Convert a dictionary or list of dictionaries into a Awkward Array

    Args:
        input_dict (dict): the dictionary to convert

    Returns:
        (Awkward): the data converted as Awkward Array
    """
    return ak.from_arrow(dict_to_pyarrow(input_dict))


def dict_to_parquet(input_dict, filename):
    """Save a given dictionary into a parquet file

    Args:
        input_dict (dict): the dictionary to save
        filename (string): the file name (with its path)
    """

    # we could also just go to pandas, and then to parquet.
    #   dict_to_pandas(input_dict).to_parquet(filename)
    name, ext = os.path.splitext(filename)
    if len(ext) == 0:
        filename = filename + ".parquet"
    pyarrow_to_parquet(dict_to_pyarrow(input_dict), filename)


def dict_to_pickle(input_dict, filename):
    """Save a given dictionary into a pickle file

    **WARNING**: Note that pickle is not the favourite way of storing data!

    Args:
        input_dict (dict): the dictionary to save
        filename (string): the file name (with its path)
    """

    name, ext = os.path.splitext(filename)
    if len(ext) == 0:
        filename = filename + ".pkl"
    with open(filename, "wb") as handle:
        pickle.dump(input_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)


def dict_to_json(input_dict, filename):
    """Save a given dictionary into a JSON file

    Function provided for convenience, but not of interest for typical use case.

    Args:
        input_dict (dict): the dictionary to save
        filename (string): the file name (with its path)
    """
    name, ext = os.path.splitext(filename)
    if len(ext) == 0:
        filename = filename + ".json"
    dict_to_pandas(input_dict).to_json(filename)


def json_to_pandas(filename):
    """Loads a JSON file as Pandas DataFrame

    Args:
        filename (string): the file name (with its path)

    Returns:
        (DataFrame): the loaded file as Pandas DataFrame
    """
    return pd.read_json(filename)


def pandas_to_dict(input_pandas):
    """Convert a Pandas DataFrame into a pyjapcscout-like dict or list of dicts in case of many records

    Args:
        input_pandas (DataFrame): the pandas DataFrame to convert

    Returns:
        (dict or list): the data converted as dict or list of dicts
    """

    output = input_pandas.to_dict("records")
    if len(output) == 1:
        output = output[0]
    return output


def awkward_to_dict(input_awkward):
    """Convert a Awkward Array data into a pyjapcscout-like dict or list of dicts in case of many records

    Args:
        input_awkward (Awkward): the Awkward Array to convert

    Returns:
        (dict or list): the data converted as dict or list of dicts
    """
    return _convert_parrow_data(ak.to_arrow(input_awkward))


def pickle_to_dict(filename):
    """Loads a pickle from file into a pyjapcscout-like dict

    Args:
        filename (string): the file name (with its path)

    Returns:
        (dict): the data loaded as dict
    """
    with open(filename, "rb") as handle:
        load_dict = pickle.load(handle)
    return load_dict


def parquet_to_dict(filename):
    """Loads a parquet file into a pyjapcscout-like dict

    Args:
        filename (string): the file name (with its path)

    Returns:
        (dict): the data loaded as dict
    """
    return pyarrow_to_dict(parquet_to_pyarrow(filename))


# between pandas and awkward
def pandas_to_awkward(input_pandas):
    """Convert a pandas DataFrame into a Awkward Array

    Args:
        input_pandas (DataFrame): the pandas DataFrame to convert

    Returns:
        (Awkward): the data converted as Awkward Array
    """
    return dict_to_awkward(pandas_to_dict(input_pandas))


def awkward_to_pandas(input_awkward):
    """Convert a pandas DataFrame into a Awkward Array

    Args:
        input_awkward (DataFrame): the Awkward Array to convert

    Returns:
        (DataFrame): the data converted as pandas DataFrame
    """
    return dict_to_pandas(awkward_to_dict)


def parquet_to_pandas(filename):
    """ It reads a **single(?)** parquet file into a pandas DataFrame with no data type loss

    Args:
        filename (string): the file name (with its path)

    Returns:
        (DataFrame): the data loaded as pandas DataFrame
    """

    return dict_to_pandas(parquet_to_dict(filename))

def parquet_to_awkward(filename):
    """ It reads a **single(?)** parquet file into a Awkward Array with no data type loss

    Args:
        filename (string): the file name (with its path)

    Returns:
        (Awkward): the data loaded as Awkward Array
    """
    return ak.from_parquet(filename)


"""
####### Simple save/load functions for the user
"""


def save_dict(dictData, folderPath=None, filename=None, fileFormat="parquet"):
    """Save data stored in a dictionary to file

    Args:
        dictData (dict): the dictionary to store
        folderPath(str): the folder/path where to store data (default=None i.e. stores in the active path)
        filename(str): the file name (default=None, i.e. it uses local time as file name)
        fileFormat(str): file format in ['parquet', 'json', 'pickle'] (default="parquet")

    """
    if filename is None:
        filename = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S.%f")
    Path(folderPath).mkdir(parents=True, exist_ok=True)
    filename = os.path.join(folderPath, filename)
    if fileFormat == "parquet":
        dict_to_parquet(dictData, filename + ".parquet")
    elif fileFormat == "json":
        dict_to_json(dictData, filename + ".json")
    elif (fileFormat == "pickle") or (fileFormat == "pickledict"):
        dict_to_pickle(dictData, filename + ".pkl")
    elif fileFormat == "mat":
        raise ValueError("MAT format not yet supported")
        scipy.io.savemat(filename + ".mat", dictData)
    else:
        raise ValueError("Unknown file format")


def load_dict(filename, fileFormat="parquet"):
    """Loads data from file into a pyjapcscout-like dictionary

    Args:
        filename (string): the file name (with its path)
        fileFormat(str): file format in ['parquet', 'json', 'pickle'] (default="parquet")

    Returns:
        (dict): the data loaded as  pyjapcscout-like dictionary
    """
    if fileFormat == "parquet":
        return parquet_to_dict(filename)
    elif (fileFormat == "pickle") or (fileFormat == "pickledict"):
        return pickle_to_dict(filename)
    elif fileFormat == "json":
        raise ValueError("JSON format not yet supported")
    elif fileFormat == "mat":
        raise ValueError("MAT format not yet supported")
        print("TODO: compatibility with MATLAB generated files?!")
        return scipy.io.loadmat(filename)
    else:
        raise ValueError("Unknown file format ({})".format(fileFormat))


"""
####### Some additional functions for debugging purposes
"""


def _find_lists(data, verbose=False):
    """
    Look inside data (assumed to be a dict) and tell if some fields are actually lists.

    In theory, `datascout` package is meant to be used only on dicts that do NOT contain any list!
    """
    for key, value in data.items():
        if verbose:
            print(key)
        if isinstance(value, list):
            print(key + " is a list!")
        elif isinstance(value, dict):
            _find_lists(value)
        else:
            if verbose:
                print(" ..is " + str(type(value)))


def _compare_data(data1, data2, use_assert=False):
    """
    Compares two dictionaries or lists and show the differences (of type or data type).

    For a full comparison, it is sometimes best to call this function also with inverted inputs.
    """

    def not_equal(a, b):
        print("   ------   ")
        print(str(a) + " (" + str(type(a)) + ")")
        print("   NOT EQUAL   ")
        print(str(b) + " (" + str(type(b)) + ")")
        print("   ------   ")
        if use_assert:
            raise AssertionError("{} not equal to {}".format(a, b))

    if (type(data1) != type(data2)) or (
        hasattr(data1, "__len__") and (len(data1) != len(data2))
    ):
        not_equal(data1, data2)
    elif isinstance(data1, list):
        for i in range(len(data1)):
            _compare_data(data1[i], data2[i], use_assert)
    elif isinstance(data1, dict):
        _compare_data(data1.keys(), data2.keys(), use_assert)
        for key in data1.keys():
            _compare_data(data1[key], data2[key], use_assert)
    elif isinstance(data1, np.ndarray):
        if data1.dtype != object:
            if not np.array_equal(data1, data2):
                not_equal(data1, data2)
        elif data1.shape == data2.shape:
            for i in range(data1.size):
                _compare_data(data1.flatten()[i], data2.flatten()[i], use_assert)
        else:
            not_equal(data1, data2)


"""
####### Additional functions for ease user experience with folders of parquet files
"""

def _compare_schema(a, reference_file = None, verbose = False):
    """
    Compare the schema of a list of file **a**. 
    The default reference file in the first one of the list **a**.
    It is possible to define a different reference file using the **reference_file**.
    """
    my_list = []
    if len(a) > 0:
        if verbose: print(f'Reference schema: {a[0]}')
        if reference_file == None:
            reference_file = a[0]
        ref_schema = pq.read_schema(reference_file)
        for ii in a:
            
            if not ref_schema==pq.read_schema(ii):
                if verbose: 
                    print(f'\n ****** {ii} has a different schema! ****** ')
                    print('This is the difference\n === Schema of the reference file ===')
                    print(set(ref_schema)-set(pq.read_schema(ii)))
                    print('This is the difference\n === Schema of the compared file ===')
                    print(set(pq.read_schema(ii))-set(ref_schema))

                my_list.append(ii)
    else:
        print('No file in the folder.')
    return my_list





def split_schemas(list_of_files):
    """
    Split a list of Parquet files by their data schema.

    This function is convenient when one needs to extract from many parquet files a sub-set
    that follows the same schema, i.e. that can be all loaded at once in a pandas DataFrame

    Args:
        list_of_files (list): the file names (with their paths) to be analysed

    Returns:
        (list): list of lists. The first list includes the files for which no schema was found.
    """
    aux=_compare_schema(list_of_files, reference_file = None, verbose = False)
    if len(aux)==0:
        return [sorted(list_of_files)]
    else:
        return [sorted(list(set(list_of_files)-set(aux))), *split_schemas(aux)]
    
def compare_schema(folder, reference_file = None, verbose = False):
    """
    Given a **folder**, it compares all parquet files in it.

    This function is convenient when one needs to extract from many parquet files a sub-set
    that follows the same schema, i.e. that can be all loaded at once in a pandas dataframe

    Args:
        folder (list): the folder name
        reference_file (str): file from which to take the reference schema (default=None, i.e. it uses the first file as reference)
        verbose (bool): (default=False)

    Returns:
        (list): returns a list of files with scema equal to the one of **reference_file**
    """
    return _compare_schema(sorted(glob.glob(folder+'/**/*.parquet', recursive=True)), 
                           reference_file, verbose)

