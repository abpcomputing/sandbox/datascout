.. _usage:

Usage
=====

Datascout provides a set of self-explained functions for data conversion, saving, loading, as well as timestamp conversion.

A simple example of use could be the following:

.. testcode::

    import datascout as ds
    import numpy as np

    # Generate a simple (nasted) dictionary
    my_dict = dict()
    my_dict['device1'] = np.random.rand(10)
    my_dict['device2'] = dict()
    my_dict['device2']['value'] = np.random.rand(10,2)
    my_dict['device2']['string'] = 'test dict'

The generated dict can now be converted to Awkward Arrays, PyArrow Arrays or pandas DataFrame:

.. testcode::

    ds.dict_to_awkward(my_dict)
    ds.dict_to_pyarrow(my_dict)
    ds.dict_to_pandas(my_dict)

The generated dict can also be stored to file:

.. testcode::

    # Parquet
    ds.dict_to_parquet(my_dict, "my_test_data.parquet")
    my_dict_load = ds.parquet_to_dict("my_test_data.parquet")
    # This will return no output if the two dictionaries contain the same data
    ds._compare_data(my_dict, my_dict_load)

    # Pickle
    ds.dict_to_pickle(my_dict, "my_test_data.pickle")
    my_dict_load = ds.pickle_to_dict("my_test_data.pickle")
    ds._compare_data(my_dict, my_dict_load)

If one decides to store data to JSON, one should know that data type and precision is
**NOT preserved**!

.. testcode::

    # JSON - no exact data preservation
    ds.dict_to_json(my_dict, "my_test_data.json")
    my_pandas_load = ds.json_to_pandas("my_test_data.json")
    my_dict_from_json = ds.pandas_to_dict(my_pandas_load)
    # In this case, differences will be shown!!! data type and precision is not ensured!
    ds._compare_data(my_dict, my_dict_from_json)

The loss of precision is due to the JSON data format, and not to data conversion to pandas

.. testcode::

    # note: going to pandas and back to dict does keep the data integrity
    my_pandas = ds.dict_to_pandas(my_dict)
    my_dict_from_pandas = ds.pandas_to_dict(my_pandas)
    # This will return no output if the two dictionaries contain the same data
    ds._compare_data(my_dict, my_dict_from_pandas)

It also provides some sweet functions to deal with timestamps (typically in ns from epoch)

.. testcode::

    import datascout as ds
    import datetime

    reference_stamp = 1645617457640238425
    reference_time  = datetime.datetime(2022, 2, 23, 12, 57, 37, 640238)

    myDateTime_utc = ds.unixtime_to_datetime(reference_stamp, time_zone = 'utc')
    myDateTime_local = ds.unixtime_to_datetime(reference_stamp, time_zone = 'local')
    myDateTime_cern = ds.unixtime_to_datetime(reference_stamp, time_zone = 'CERN')
    myDateTime_unaware = ds.unixtime_to_datetime(reference_stamp, time_zone = None)

    reference_time_str = ds.unixtime_to_string(reference_stamp, time_zone = 'CERN')
    print(reference_time_str)

.. parsed-literal::

    2022-02-23 12:57:37.640238



