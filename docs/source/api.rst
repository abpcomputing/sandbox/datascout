.. _API_docs:

datascout API documentation
============================

.. rubric:: Modules

.. autosummary::
   :toctree: api

   .. Add the sub-packages that you wish to document below

   datascout
