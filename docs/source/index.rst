datascout
=========

Introduction
------------

Datascout is a simple package to handle data saving and reading with minimum required libraries.
The main purpose is to use it as dependance of `pyjapcscout` in the control room for storing data
acquired form the control system as parquet files, and then on the user's "GPN" computer for data
analysis without the need of JAVA or other dependances needed to interact with the control system.

The basic data unit (or dataset) is assumed to be a (nested) **dictionary** of **numpy values** and **numpy arrays**.
**Lists** are in principle not allowed (at least not supported) inside a dataset.
On the other hand, **lists** might be used to define a list of datasets (e.g. a list of consecutive acquisitions of a accelerator data).

This package provides the following (main) functions. Note that many of those functions are simple wrappers of external functions (from `pandas`, `pyarrow`, `awkward`), but sometimes with some twiks to make sure data type/shape is somewhat always preserved.

- `dict_to_pandas(input_dict)`: Creates a `pandas` dataframe from a (list of) `dict`.
- `dict_to_awkward(input_dict)`: Creates an `awkward` array from a (list of) `dict`.
- `dict_to_parquet(input_dict, filename)`: Saves a (list of) `dict` into a `parquet` file. **In order to do so, 2D arrays are split in 1D arrays of 1D arrays.**
- `dict_to_pickle(input_dict, filename)`: Saves a (list of) `dict` into a `pickle` file.
- `dict_to_json(input_dict, filename)`: Saves a (list of) `dict` into a `json` file.
- `json_to_pandas(filename)`: It loads from a `json` file a `pandas` dataframe. This function is not so interesting (because data types/shapes are not preserved), but provided for convenience.
- `pandas_to_dict(input_pandas)`: It converts back a `pandas` dataframe into a (list of) `dict`.
- `awkward_to_dict(input_awkward)`: It converts back a `awkward` array into a (list of) `dict`. **In order to preserve data type/shape, it re-merges 1D arrays of 1D arrays into 2D arrays.**
- `parquet_to_dict(filename)`: Loads a (list of) `dict` from a `parquet` file. **In order to preserve data type/shape, it re-merges 1D arrays of 1D arrays into 2D arrays.**
- `pickle_to_dict(filname)`: Loads a (list of) `dict` from a `pickle` file.
- `pandas_to_awkward(input_pandas)`: It creates an `awkward`array starting from a `pandas` dataframe.
- `awkward_to_pandas(input_awkward)`: It creates an `pandas` dataframe starting from a `awkward` array.
- `parquet_to_pandas(filename)`: It loads a `parquet` file into a `pandas` dataframe. **Instead of using the method provided by `pandas` (which does not preserve single value types and 2D arrays), it first loads the parquet as `dict`, and then converts it into a `pandas` dataframe.**
- `parquet_to_awkward(filename)`: It loads a `parquet` file into a `awkward` array.
- `save_dict(dictData, folderPath = None, filename = None, fileFormat='parquet')`: Additional wrapper of a few functions above to easily save a `dict` on a file using a supported format (`parquet` and `dict` for the time being)
- `load_dict(filename, fileFormat='parquet')`: It reads a file assuming a given format and returns its content as a `dict` (which can be then converted to other formats...)


Installation
------------

Using the `acc-py Python package index
<https://wikis.cern.ch/display/ACCPY/Getting+started+with+acc-python#Gettingstartedwithacc-python-OurPythonPackageRepositoryrepo>`_
``datascout`` can be pip installed with::

   pip install datascout

If you prefer to install your own local development version, you can simply clone the repository::

    git clone https://gitlab.cern.ch/abpcomputing/sandbox/datascout.git datascout
    cd datascout
    python -m pip install -e .


Documentation contents
----------------------

.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: datascout
    :maxdepth: 1

    usage

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex

